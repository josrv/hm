# hm
[![Build status](https://gitlab.com/josrv/hm/badges/master/pipeline.svg)](https://gitlab.com/josrv/hm/commits/master)

DuckDuckGo CLI interface

## Usage
Launch `hm`. An interactive prompt will appear allowing you to issue search queries.

When the search results are shown, the following control commands are available:  
**Esc** start a new search  
**n** load the next page  
**p** load the previous page  
**q** quit

To select one of the results, type its result key. 

## Configuration
When hm starts, it reads configuration options from `$XDG_CONFIG_HOME/hm/config`. Custom config file location can be specified
using the `-c` command-line option. If no config file is provided, hm uses the default config options (see below).

Supported configuration options:
limit=9  
The number of results per page

url=https://duckduckgo.com    
The url of the search engine

keys=asdfghjkl  
Symbols to use to form result keys

command=xdg-open  
Command that is launched when a link is selected. The link url is passed as a first positional argument.

## Non-interactive mode
hm can be used non-interactively using the -q command-line option: `hm -q <search term>`. In this mode the program just lists results and exits.
