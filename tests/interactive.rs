#![feature(proc_macro_hygiene)]

use rexpect::errors::*;
use mockito::{mock, server_url, Matcher};
use hm::dto::Link;
use hm::parser::Paging;
use ini::Ini;
use rexpect::session::PtySession;

mod common;

const ESC: char = '[';
const PROMPT: &str = "ddg> ";

fn send(session: &mut PtySession, input: &str) -> Result<()> {
    session.send(input)?;
    session.flush()
}

mod search {
    use super::*;

    #[test]
    fn search_one_query_and_exit() -> Result<()> {
        let html = common::generate_response(vec![
            Link {
                url: "http://test.com/1".to_owned(),
                title: "Test link 1".to_owned(),
                snippet: "Test snippet 1".to_owned(),
            },
            Link {
                url: "http://test.com/2".to_owned(),
                title: "Test link 2".to_owned(),
                snippet: "Test snippet 2".to_owned(),
            }
        ], None, None);

        let _m = mock("POST", "/html")
            .match_body(Matcher::Regex("q=test".into()))
            .with_status(200)
            .with_body(html)
            .create();

        let url = server_url();
        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("url", url);

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("test")?;
            p.exp_string("A 1. Test link 1 | http://test.com/1")?;
            p.exp_string("Test snippet 1")?;
            p.exp_string("S 2. Test link 2 | http://test.com/2")?;
            p.exp_string("Test snippet 2")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }

    #[test]
    fn search_two_queries_and_exit() -> Result<()> {
        let first_html = common::generate_response(vec![
            Link {
                url: "http://first.com/".to_owned(),
                title: "First link".to_owned(),
                snippet: "First snippet".to_owned(),
            }
        ], None, None);

        let second_html = common::generate_response(vec![
            Link {
                url: "http://second.com/".to_owned(),
                title: "Second link".to_owned(),
                snippet: "Second snippet".to_owned(),
            }
        ], None, None);

        let _first_m = mock("POST", "/html")
            .match_body(Matcher::Regex("q=first".into()))
            .with_status(200)
            .with_body(first_html)
            .create();

        let _second_m = mock("POST", "/html")
            .match_body(Matcher::Regex("q=second".into()))
            .with_status(200)
            .with_body(second_html)
            .create();

        let url = server_url();
        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("url", url);

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("first")?;
            p.exp_string("A 1. First link | http://first.com/")?;
            p.exp_string("First snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            p.send_control(ESC)?;
            p.flush()?;
            p.exp_string(PROMPT)?;
            p.send_line("second")?;
            p.exp_string("A 1. Second link | http://second.com/")?;
            p.exp_string("Second snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }

    #[test]
    fn shows_error_and_prompt_if_host_is_not_available() -> Result<()> {
        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("url", "INVALID");

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("test")?;
            p.exp_string("Couldn't resolve host name")?;
            p.exp_string(PROMPT)?;
            Ok(())
        })
    }

    #[test]
    fn prints_links_even_if_response_size_is_less_than_limit() -> Result<()> {
        let html = common::generate_response(vec![
            Link {
                url: "http://first.com/".to_owned(),
                title: "First link".to_owned(),
                snippet: "First snippet".to_owned(),
            }
        ], None, None);

        let _m = mock("POST", "/html")
            .with_status(200)
            .with_body(html)
            .create();

        let url = server_url();
        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("url", url)
            .set("limit", "2");

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("first")?;
            p.exp_string("A 1. First link | http://first.com/")?;
            p.exp_string("First snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }
}

mod selection {
    /// Tests in this section set "echo" as the command and use its output in assertions
    use super::*;

    #[test]
    fn select_link_and_exit() -> Result<()> {
        let html = common::generate_response(vec![
            Link {
                url: "http://test.com/1".to_owned(),
                title: "Test link 1".to_owned(),
                snippet: "Test snippet 1".to_owned(),
            },
            Link {
                url: "http://test.com/2".to_owned(),
                title: "Test link 2".to_owned(),
                snippet: "Test snippet 2".to_owned(),
            }
        ], None, None);

        let _m = mock("POST", "/html")
            .match_body(Matcher::Regex("q=test".into()))
            .with_status(200)
            .with_body(html)
            .create();

        let url = server_url();
        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("command", "echo")
            .set("keys", "abc")
            .set("url", url);

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("test")?;
            p.exp_string("A 1. Test link 1 | http://test.com/1")?;
            p.exp_string("Test snippet 1")?;
            p.exp_string("B 2. Test link 2 | http://test.com/2")?;
            p.exp_string("Test snippet 2")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "a")?;
            p.exp_string("http://test.com/1")?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }

    #[test]
    fn select_three_links_and_exit() -> Result<()> {
        let html = common::generate_response(vec![
            Link {
                url: "http://test.com/1".to_owned(),
                title: "Test link 1".to_owned(),
                snippet: "Test snippet 1".to_owned(),
            },
            Link {
                url: "http://test.com/2".to_owned(),
                title: "Test link 2".to_owned(),
                snippet: "Test snippet 2".to_owned(),
            }
        ], None, None);

        let _m = mock("POST", "/html")
            .match_body(Matcher::Regex("q=test".into()))
            .with_status(200)
            .with_body(html)
            .create();

        let url = server_url();
        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("command", "echo")
            .set("keys", "abc")
            .set("url", url);

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("test")?;
            p.exp_string("A 1. Test link 1 | http://test.com/1")?;
            p.exp_string("Test snippet 1")?;
            p.exp_string("B 2. Test link 2 | http://test.com/2")?;
            p.exp_string("Test snippet 2")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "a")?;
            p.exp_string("http://test.com/1")?;
            send(&mut p, "b")?;
            p.exp_string("http://test.com/2")?;
            send(&mut p, "a")?;
            p.exp_string("http://test.com/1")?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }

    #[test]
    fn select_link_multichar_key() -> Result<()> {
        let html = common::generate_response(vec![
            Link {
                url: "http://test.com/1".to_owned(),
                title: "Test link 1".to_owned(),
                snippet: "Test snippet 1".to_owned(),
            },
            Link {
                url: "http://test.com/2".to_owned(),
                title: "Test link 2".to_owned(),
                snippet: "Test snippet 2".to_owned(),
            },
            Link {
                url: "http://test.com/3".to_owned(),
                title: "Test link 3".to_owned(),
                snippet: "Test snippet 3".to_owned(),
            }
        ], None, None);

        let _m = mock("POST", "/html")
            .match_body(Matcher::Regex("q=test".into()))
            .with_status(200)
            .with_body(html)
            .create();

        let url = server_url();
        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("command", "echo")
            .set("keys", "ab")
            .set("url", url);

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("test")?;
            p.exp_string("AA 1. Test link 1 | http://test.com/1")?;
            p.exp_string("Test snippet 1")?;
            p.exp_string("AB 2. Test link 2 | http://test.com/2")?;
            p.exp_string("Test snippet 2")?;
            p.exp_string("BA 3. Test link 3 | http://test.com/3")?;
            p.exp_string("Test snippet 3")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "ab")?;
            p.exp_string("http://test.com/2")?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }

    #[test]
    fn should_ignore_unrelated_chars() -> Result<()> {
        let html = common::generate_response(vec![
            Link {
                url: "http://test.com/1".to_owned(),
                title: "Test link 1".to_owned(),
                snippet: "Test snippet 1".to_owned(),
            },
            Link {
                url: "http://test.com/2".to_owned(),
                title: "Test link 2".to_owned(),
                snippet: "Test snippet 2".to_owned(),
            },
        ], None, None);

        let _m = mock("POST", "/html")
            .match_body(Matcher::Regex("q=test".into()))
            .with_status(200)
            .with_body(html)
            .create();

        let url = server_url();
        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("command", "echo")
            .set("keys", "abc")
            .set("url", url);

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("test")?;
            p.exp_string("A 1. Test link 1 | http://test.com/1")?;
            p.exp_string("Test snippet 1")?;
            p.exp_string("B 2. Test link 2 | http://test.com/2")?;
            p.exp_string("Test snippet 2")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "cda")?;
            p.exp_string("http://test.com/1")?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }

    #[test]
    fn should_discard_wrong_keys_and_continue_waiting_for_new_ones() -> Result<()> {
        let html = common::generate_response(vec![
            Link {
                url: "http://test.com/1".to_owned(),
                title: "Test link 1".to_owned(),
                snippet: "Test snippet 1".to_owned(),
            },
            Link {
                url: "http://test.com/2".to_owned(),
                title: "Test link 2".to_owned(),
                snippet: "Test snippet 2".to_owned(),
            },
            Link {
                url: "http://test.com/3".to_owned(),
                title: "Test link 3".to_owned(),
                snippet: "Test snippet 3".to_owned(),
            }
        ], None, None);

        let _m = mock("POST", "/html")
            .match_body(Matcher::Regex("q=test".into()))
            .with_status(200)
            .with_body(html)
            .create();

        let url = server_url();
        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("command", "echo")
            .set("keys", "ab")
            .set("url", url);

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("test")?;
            p.exp_string("AA 1. Test link 1 | http://test.com/1")?;
            p.exp_string("Test snippet 1")?;
            p.exp_string("AB 2. Test link 2 | http://test.com/2")?;
            p.exp_string("Test snippet 2")?;
            p.exp_string("BA 3. Test link 3 | http://test.com/3")?;
            p.exp_string("Test snippet 3")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "bb")?;
            send(&mut p, "ab")?;
            p.exp_string("http://test.com/2")?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }
}

mod paging {
    use super::*;

    /// First page returns 1 link, limit is 1. To get more results, one additional fetch request
    /// should be issued.
    #[test]
    fn fetches_next_page_limit_equal_to_response_size() -> Result<()> {
        let first_page = common::generate_response(vec![
            Link {
                url: "http://first.com/".to_owned(),
                title: "First link".to_owned(),
                snippet: "First snippet".to_owned(),
            }
        ], None, Some(Paging { s: 20, dc: 20 }));

        let second_page = common::generate_response(vec![
            Link {
                url: "http://second.com/".to_owned(),
                title: "Second link".to_owned(),
                snippet: "Second snippet".to_owned(),
            }
        ], Some(Paging { s: 10, dc: 10 }), Some(Paging { s: 30, dc: 30 }));

        let _first_m = mock("POST", "/html")
            .with_status(200)
            .with_body(first_page)
            .create();

        let _second_m = mock("POST", "/html")
            .match_body(Matcher::Regex("s=20".into()))
            .with_status(200)
            .with_body(second_page)
            .create();

        let url = server_url();

        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("url", url)
            .set("limit", "1");

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("first")?;
            p.exp_string("A 1. First link | http://first.com/")?;
            p.exp_string("First snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "n")?;
            p.exp_string("A 1. Second link | http://second.com/")?;
            p.exp_string("Second snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }

    /// First page returns 2 links, limit is 1. To get more results, no additional fetch requests
    /// should be issued.
    #[test]
    fn fetches_next_page_limit_less_than_response_size() -> Result<()> {
        let first_page = common::generate_response(vec![
            Link {
                url: "http://first.com/".to_owned(),
                title: "First link".to_owned(),
                snippet: "First snippet".to_owned(),
            },
            Link {
                url: "http://second.com/".to_owned(),
                title: "Second link".to_owned(),
                snippet: "Second snippet".to_owned(),
            }
        ], None, Some(Paging { s: 20, dc: 20 }));

        let _first_m = mock("POST", "/html")
            .match_body(Matcher::Regex("q=first".into()))
            .with_status(200)
            .with_body(first_page)
            .create();

        let url = server_url();
        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("url", url)
            .set("limit", "1");

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("first")?;
            p.exp_string("A 1. First link | http://first.com/")?;
            p.exp_string("First snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "n")?;
            p.exp_string("A 1. Second link | http://second.com/")?;
            p.exp_string("Second snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }

    /// Each page contains 1 link, limit is 2. Two fetch requests should be issued to get data
    /// for each query.
    #[test]
    fn fetches_next_page_limit_more_than_response_size() -> Result<()> {
        let first_page = common::generate_response(vec![
            Link {
                url: "http://first.com/".to_owned(),
                title: "First link".to_owned(),
                snippet: "First snippet".to_owned(),
            }
        ], None, Some(Paging { s: 20, dc: 20 }));

        let second_page = common::generate_response(vec![
            Link {
                url: "http://second.com/".to_owned(),
                title: "Second link".to_owned(),
                snippet: "Second snippet".to_owned(),
            }
        ], Some(Paging { s: 10, dc: 10 }), Some(Paging { s: 30, dc: 30 }));

        let third_page = common::generate_response(vec![
            Link {
                url: "http://third.com/".to_owned(),
                title: "Third link".to_owned(),
                snippet: "Third snippet".to_owned(),
            }
        ], Some(Paging { s: 20, dc: 20 }), Some(Paging { s: 40, dc: 40 }));

        let fourth_page = common::generate_response(vec![
            Link {
                url: "http://fourth.com/".to_owned(),
                title: "Fourth link".to_owned(),
                snippet: "Fourth snippet".to_owned(),
            }
        ], Some(Paging { s: 30, dc: 30 }), Some(Paging { s: 50, dc: 50 }));

        let _first_m = mock("POST", "/html")
            .with_status(200)
            .with_body(first_page)
            .create();

        let _second_m = mock("POST", "/html")
            .match_body(Matcher::Regex("s=20".into()))
            .with_status(200)
            .with_body(second_page)
            .create();

        let _third_m = mock("POST", "/html")
            .match_body(Matcher::Regex("s=30".into()))
            .with_status(200)
            .with_body(third_page)
            .create();

        let _fourth_m = mock("POST", "/html")
            .match_body(Matcher::Regex("s=40".into()))
            .with_status(200)
            .with_body(fourth_page)
            .create();

        let url = server_url();
        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("url", url)
            .set("limit", "2");

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("first")?;
            p.exp_string("A 1. First link | http://first.com/")?;
            p.exp_string("First snippet")?;
            p.exp_string("S 2. Second link | http://second.com/")?;
            p.exp_string("Second snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "n")?;
            p.exp_string("A 1. Third link | http://third.com/")?;
            p.exp_string("Third snippet")?;
            p.exp_string("S 2. Fourth link | http://fourth.com/")?;
            p.exp_string("Fourth snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }

    #[test]
    fn should_ignore_next_page_command_if_there_are_no_more_pages() -> Result<()> {
        let first_page = common::generate_response(vec![
            Link {
                url: "http://first.com/".to_owned(),
                title: "First link".to_owned(),
                snippet: "First snippet".to_owned(),
            }
        ], None, Some(Paging { s: 20, dc: 20 }));

        let second_page = common::generate_response(vec![
            Link {
                url: "http://second.com/".to_owned(),
                title: "Second link".to_owned(),
                snippet: "Second snippet".to_owned(),
            }
        ], Some(Paging { s: 10, dc: 10 }), None);

        let _first_m = mock("POST", "/html")
            .with_status(200)
            .with_body(first_page)
            .create();

        let _second_m = mock("POST", "/html")
            .match_body(Matcher::Regex("s=20".into()))
            .with_status(200)
            .with_body(second_page)
            .create();

        let url = server_url();
        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("url", url)
            .set("limit", "1");

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("first")?;
            p.exp_string("A 1. First link | http://first.com/")?;
            p.exp_string("First snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "n")?;
            p.exp_string("A 1. Second link | http://second.com/")?;
            p.exp_string("Second snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "n")?;
            p.exp_char('\n')?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }

    #[test]
    fn should_fetch_next_page_and_then_navigate_back() -> Result<()> {
        let first_page = common::generate_response(vec![
            Link {
                url: "http://first.com/".to_owned(),
                title: "First link".to_owned(),
                snippet: "First snippet".to_owned(),
            }
        ], None, Some(Paging { s: 20, dc: 20 }));

        let second_page = common::generate_response(vec![
            Link {
                url: "http://second.com/".to_owned(),
                title: "Second link".to_owned(),
                snippet: "Second snippet".to_owned(),
            }
        ], Some(Paging { s: 10, dc: 10 }), Some(Paging { s: 30, dc: 30 }));

        let _first_m = mock("POST", "/html")
            .with_status(200)
            .with_body(first_page)
            .create();

        let _second_m = mock("POST", "/html")
            .match_body(Matcher::Regex("s=20".into()))
            .with_status(200)
            .with_body(second_page)
            .create();

        let url = server_url();

        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("url", url)
            .set("limit", "1");

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("first")?;
            p.exp_string("A 1. First link | http://first.com/")?;
            p.exp_string("First snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "n")?;
            p.exp_string("A 1. Second link | http://second.com/")?;
            p.exp_string("Second snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "p")?;
            p.exp_string("A 1. First link | http://first.com/")?;
            p.exp_string("First snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }

    #[test]
    fn should_ignore_previous_page_command_if_offset_is_zero() -> Result<()> {
        let first_page = common::generate_response(vec![
            Link {
                url: "http://first.com/".to_owned(),
                title: "First link".to_owned(),
                snippet: "First snippet".to_owned(),
            }
        ], None, Some(Paging { s: 20, dc: 20 }));

        let second_page = common::generate_response(vec![
            Link {
                url: "http://second.com/".to_owned(),
                title: "Second link".to_owned(),
                snippet: "Second snippet".to_owned(),
            }
        ], Some(Paging { s: 10, dc: 10 }), Some(Paging { s: 30, dc: 30 }));

        let _first_m = mock("POST", "/html")
            .with_status(200)
            .with_body(first_page)
            .create();

        let _second_m = mock("POST", "/html")
            .match_body(Matcher::Regex("s=20".into()))
            .with_status(200)
            .with_body(second_page)
            .create();

        let url = server_url();

        let mut conf = Ini::new();
        conf.with_section(None::<String>)
            .set("url", url)
            .set("limit", "1");

        common::run(conf, |config_path| {
            let mut p = common::launch_interactive(config_path)?;
            p.exp_string(PROMPT)?;
            p.send_line("first")?;
            p.exp_string("A 1. First link | http://first.com/")?;
            p.exp_string("First snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "n")?;
            p.exp_string("A 1. Second link | http://second.com/")?;
            p.exp_string("Second snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "p")?;
            p.exp_string("A 1. First link | http://first.com/")?;
            p.exp_string("First snippet")?;
            p.exp_string("Type result key or <Esc> to start a new search")?;
            send(&mut p, "p")?;
            p.exp_char('\n')?;
            send(&mut p, "q")?;
            p.exp_eof()?;

            Ok(())
        })
    }
}
