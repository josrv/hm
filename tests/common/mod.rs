use hm::dto::Link;
use ini::Ini;
use maud::html;
use rexpect::errors::*;
use rexpect::session::PtySession;
use std::env::temp_dir;
use std::fs;
use std::fs::File;
use std::path::{Path};
use std::time::{SystemTime, UNIX_EPOCH};
use hm::parser::Paging;

pub fn launch_interactive(config_path: &Path) -> Result<PtySession> {
    let cmd = format!("./target/debug/hm -c {}", config_path.display());
    spawn(cmd.as_str())
}

pub fn launch_noninteractive(config_path: &Path, query: &str) -> Result<PtySession> {
    let cmd = format!(
        "./target/debug/hm -c {} -q {}",
        config_path.display(),
        query
    );
    spawn(cmd.as_str())
}

fn spawn(cmd: &str) -> Result<PtySession> {
    rexpect::spawn(cmd, Some(1000))
}

pub fn run<F>(config: Ini, f: F) -> Result<()>
    where F: FnOnce(&Path) -> Result<()>,
{
    let config_file_name = format!(
        "hm-test-config-{}",
        SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_micros()
    );
    let config_path = temp_dir().join(config_file_name);
    File::create(&config_path).unwrap();
    config.write_to_file(&config_path).unwrap();

    let test_result = f(&config_path.as_path());

    fs::remove_file(config_path).unwrap();

    test_result
}

pub fn generate_response(links: Vec<Link>, prev_form: Option<Paging>, next_form: Option<Paging>) -> String {
    let output = html! {
        html {
            body {
                div#links.results {
                    @for link in &links {
                        div.result {
                            div.result__body {
                                a.result__a href=(link.url) { (link.title) }
                                a.result__snippet { (link.snippet) }
                            }
                        }
                    }
                    @if let Some(form) = prev_form {
                        div.nav-link {
                            form {
                                input type="submit" value="Previous" {}
                                input name="s" value=(form.s) {}
                                input name="dc" value=(form.dc) {}
                            }
                        }
                    }

                    @if let Some(form) = next_form {
                        div.nav-link {
                            form {
                                input type="submit" value="Next" {}
                                input name="s" value=(form.s) {}
                                input name="dc" value=(form.dc) {}
                            }
                        }
                    }
                }
            }
        }
    };

    output.into_string()
}
