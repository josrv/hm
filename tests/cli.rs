#![feature(proc_macro_hygiene)]

use rexpect::errors::Result;
use mockito::{mock, server_url, Matcher};
use hm::dto::Link;
use ini::Ini;

mod common;

#[test]
fn search_one_query() -> Result<()> {
    let html = common::generate_response(vec![
        Link {
            url: "http://test.com/1".to_owned(),
            title: "Test link 1".to_owned(),
            snippet: "Test snippet 1".to_owned(),
        },
        Link {
            url: "http://test.com/2".to_owned(),
            title: "Test link 2".to_owned(),
            snippet: "Test snippet 2".to_owned(),
        }
    ], None, None);

    let _m = mock("POST", "/html")
        .match_body(Matcher::Regex("q=test".into()))
        .with_status(200)
        .with_body(html)
        .create();

    let url = server_url();
    let mut conf = Ini::new();
    conf.with_section(None::<String>)
        .set("url", url);

    common::run(conf, |config_path| {
        let mut p = common::launch_noninteractive(config_path, "test")?;
        p.exp_string("1. Test link 1 | http://test.com/1")?;
        p.exp_string("Test snippet 1")?;
        p.exp_string("2. Test link 2 | http://test.com/2")?;
        p.exp_string("Test snippet 2")?;
        p.exp_eof()?;

        Ok(())
    })
}

#[test]
fn shows_error_if_host_is_not_available() -> Result<()> {
    let mut conf = Ini::new();
    conf.with_section(None::<String>)
        .set("url", "INVALID");

    common::run(conf, |config_path| {
        let mut p = common::launch_noninteractive(config_path, "test")?;
        p.exp_string("Couldn't resolve host name")?;
        p.exp_eof()?;
        Ok(())
    })
}
