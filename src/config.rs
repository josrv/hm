use ini::Ini;
use std::io::Read;
use crate::utils::Alphabet;

#[derive(Debug)]
pub struct Config {
    pub url: String,
    pub limit: u8,
    pub keys: Alphabet,
    pub command: String,
}

const DEFAULT_URL: &str = "https://duckduckgo.com";
const DEFAULT_LIMIT: u8 = 9;
//home row
const DEFAULT_KEYS: &str = "asdfghjkl";
const DEFAULT_COMMAND: &str = "xdg-open";

impl Default for Config {
    fn default() -> Self {
        Config {
            url: DEFAULT_URL.to_owned(),
            limit: DEFAULT_LIMIT,
            keys: Alphabet::from(DEFAULT_KEYS),
            command: DEFAULT_COMMAND.to_owned(),
        }
    }
}

impl Config {
    pub fn from<R>(mut reader: R) -> Result<Self, String>
        where R: Read {
        let mut conf = Ini::read_from(&mut reader)
            .map_err(|e| {
                e.to_string()
            })?;

        let mut config = Config::default();
        let mut general_section = conf.delete(None::<String>).unwrap();

        if let Some(mut value) = general_section.remove("url") {
            config.url = value.drain(..).collect();
        }

        if let Some(mut value) = general_section.remove("command") {
            config.command = value.drain(..).collect();
        }

        if let Some(mut value) = general_section.remove("keys") {
            let alphabet: String = value.drain(..).collect();
            config.keys = Alphabet::from(alphabet.as_str());
        }

        if let Some(limit) = general_section.remove("limit") {
            config.limit = limit.parse().unwrap()
        }
        Ok(config)
    }
}

#[cfg(test)]
mod tests {
    use std::fs::File;
    use std::io::Write;
    use crate::config::Config;
    use super::*;
    use std::{env, fs};

    #[test]
    fn new_value() {
        let default = Config::default();
        assert_ne!("http://127.0.0.1", default.url);

        let simple = r#"
        url=http://127.0.0.1
        "#;

        let config = Config::from(simple.as_bytes()).unwrap();

        assert_eq!("http://127.0.0.1", config.url);
    }

    #[test]
    fn all_values() {
        let default = Config::default();
        assert_ne!("http://127.0.0.1", default.url);

        let simple = r#"
        url=http://127.0.0.1
        limit=5
        command=echo
        keys=abcdefghij
        "#;

        let config = Config::from(simple.as_bytes()).unwrap();

        assert_eq!("http://127.0.0.1", config.url);
        assert_eq!(5, config.limit);
        assert_eq!("echo", config.command);
        assert_eq!(Alphabet::from("abcdefghij"), config.keys);
    }

    #[test]
    fn default_values() {
        let default = Config::default();

        let simple = r#"
        url=http://127.0.0.1
        "#;

        let config = Config::from(simple.as_bytes()).unwrap();

        assert_eq!(default.keys, config.keys);
        assert_eq!(default.command, config.command);
        assert_eq!(default.limit, config.limit);
    }

    #[test]
    fn from_file() {
        let path = env::temp_dir().join("config");
        let mut file = File::create(path.clone()).unwrap();
        file.write_all(b"url=http://127.0.0.1").unwrap();

        let config = Config::from(File::open(path.clone()).unwrap()).unwrap();

        assert_eq!("http://127.0.0.1", config.url);

        fs::remove_file(path).unwrap();
    }
}