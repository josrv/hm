use std::env;

use getopts::Options;

use dirs::config_dir;
use hm::config::Config;
use hm::fetcher::LinkFetcher;
use hm::http::CurlHttp;
use hm::viewer::Viewer;
use std::fs::File;
use std::path::PathBuf;
use std::sync::{mpsc, Arc};
use hm::parser::Links;
use hm::display::LinksRepr;

fn main() {
    let args: Vec<String> = env::args().collect();

    let mut opts = Options::new();
    opts.optopt("q",
                "query",
                "perform a search, list results and exit",
                "QUERY",
    );
    opts.optopt("c", "config", "config file location", "PATH");
    opts.optflag("h", "help", "show this help");

    let matches = match opts.parse(&args[1..]) {
        Ok(m) => m,
        Err(f) => panic!(f.to_string()),
    };

    if matches.opt_present("h") {
        print!("{}", opts.usage("Usage: hm [options]"));
        return;
    }

    let path_arg: Result<Option<PathBuf>, _> = matches.opt_get("c");
    let config_path = path_arg.ok()
        .and_then(|c| c)
        .or_else(|| config_dir().map(|d| d.join("hm/config")))
        .ok_or_else(|| "Can't get config file".to_string());

    let config = Arc::new(config_path
        .and_then(|path| File::open(path).map_err(|e| e.to_string()))
        .and_then(Config::from)
        .unwrap_or_default());

    let (ui_tx, ui_rx) = mpsc::channel();
    let (ctrl_tx, ctrl_rx) = mpsc::channel();

    if matches.opt_present("q") {
        let query_arg: Result<Option<String>, _> = matches.opt_get("q");
        match query_arg {
            Ok(Some(query)) => {
                let http = CurlHttp::new();

                let links = LinkFetcher::fetch(&http, query.as_str(), None, &config);
                match links {
                    Ok(Links { ref links, .. }) => {
                        print!("{}", LinksRepr { links: links.as_slice(), keys: None });
                    }
                    Err(e) => {
                        panic!("{}", e)
                        //TODO handle error
                    }
                }
            }

            _ => panic!("Can't parse the argument to option 'q'"),
        }
    } else {
        let viewer_config = Arc::clone(&config);

        LinkFetcher::start(config, ctrl_rx, ui_tx);

        let mut viewer = Viewer::new(ctrl_tx);
        viewer.start(viewer_config, ui_rx);
    };
}
