#[derive(Debug, Clone, PartialEq)]
pub struct Link {
    pub title: String,
    pub url: String,
    pub snippet: String,
}
