use crate::dto::Link;
use std::fmt;

pub struct LinksRepr<'a> {
    pub links: &'a [Link],
    pub keys: Option<Vec<String>>,
}

impl<'a> fmt::Display for LinksRepr<'a> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        self.links.iter().enumerate().map(|(index, link)| {
            match &self.keys {
                Some(keys) => {
                    writeln!(f, "{} {}. {} | {}\n{}", keys[index].to_uppercase(), index + 1, link.title, link.url, link.snippet)
                }
                None => {
                    writeln!(f, "{}. {} | {}\n{}", index + 1, link.title, link.url, link.snippet)
                }
            }
        }).collect::<Result<_, _>>()
    }
}