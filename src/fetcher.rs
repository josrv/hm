use crate::dto::Link;
use crate::http::Http;
use crate::parser::{Html, Links, Paging};
use std::convert::TryFrom;
use crate::config::Config;
use std::sync::{mpsc, Arc};
use std::thread;
use std::collections::HashMap;
use crate::http::CurlHttp;
use std::cmp::min;

#[derive(Debug)]
pub enum Command {
    Query { query: String, offset: u16 },
    Abort,
}

pub struct FetchResult {
    pub links: Vec<Link>,
    pub has_more: bool,
}

pub struct LinkFetcher {}

impl LinkFetcher {
    pub fn start(
        config: Arc<Config>,
        ctrl_rx: mpsc::Receiver<Command>,
        ui_tx: mpsc::Sender<Result<FetchResult, String>>,
    ) {
        let (worker_tx, worker_rx) = mpsc::channel();

        let limit = config.limit as usize;

        let _worker = thread::spawn(move || {
            let http = CurlHttp::new();
            let mut links: HashMap<String, (Vec<Link>, Option<Paging>)> = HashMap::new();
            'worker: for r in worker_rx {
                if let Command::Query { query, offset } = r {
                    let query = query.trim();

                    let (ref mut links, ref mut paging) =
                        links.entry(query.to_string()).or_default();
                    let from = offset as usize;
                    let to = from + limit as usize;
                    let mut has_more = true;

                    // fetch more pages until there are enough to fulfil the offset request, or
                    // there are no more pages
                    while links.len() < to {
                        let fetched = Self::fetch(
                            &http,
                            query,
                            paging.as_ref(),
                            &config,
                        );
                        match fetched {
                            Ok(Links { links: next_links, paging: next_paging, .. }) => {
                                links.extend(next_links);
                                *paging = next_paging;

                                if *paging == None {
                                    // no more pages to load
                                    has_more = false;
                                    break;
                                }
                            }
                            Err(s) => {
                                ui_tx.send(Err(s)).unwrap();
                                continue 'worker;
                            }
                        }
                    }

                    let to = min(to, links.len());
                    let fetch_result = FetchResult {
                        links: links[from..to].to_vec(),
                        has_more,
                    };
                    ui_tx.send(Ok(fetch_result)).unwrap();
                }
            }
        });

        let _ctrl = thread::spawn(move || {
            for received in ctrl_rx {
                match received {
                    c @ Command::Query { .. } => {
                        worker_tx.send(c).unwrap();
                    }
                    Command::Abort => {
                        unimplemented!()
                    }
                }
            }
        });
    }

    pub fn fetch<H>(http: &H, query: &str, paging: Option<&Paging>, config: &Config) -> Result<Links, String>
        where H: Http
    {
        let url = format!("{}/html", config.url);

        //TODO reuse static params
        let query = query.trim();
        let mut data = hash_map! {
            "q" => query,
            "nextParams" => "",
            "v" => "l",
            "o" => "json",
            "api" => "/d.js"
        };

        let mut s_value;
        let mut dc_value;
        if let Some(Paging { s, dc }) = paging {
            s_value = s.to_string();
            dc_value = dc.to_string();
            data.insert("s", s_value.as_str());
            data.insert("dc", dc_value.as_str());
        };

        http.post(url.as_str(), data)
            .and_then(|r| {
                Links::try_from(Html(r.as_slice()))
            })
    }
}
