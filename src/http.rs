use std::collections::HashMap;

use curl::easy::{Easy, ReadError};
use std::io::Read;
use std::cell::Cell;
use curl::Error;
use std::error;

pub trait Http {
    fn post(&self, url: &str, data: HashMap<&str, &str>) -> Result<Vec<u8>, String>;
    fn abort(&self);
}

#[derive(Default)]
pub struct CurlHttp {
    pending_abort: Cell<bool>
}

impl CurlHttp {
    pub fn new() -> Self {
        CurlHttp {
            pending_abort: Cell::new(false)
        }
    }

    fn is_whitespace(c: char) -> bool {
        (&c).is_ascii_whitespace()
    }

    fn url_encode(data: HashMap<&str, &str>) -> String {
        data.into_iter().map(|(key, value)| {
            format!("{}={}", key, value.trim().replace(Self::is_whitespace, "+"))
        }).collect::<Vec<String>>().join("&")
    }

    fn post_inner(&self, url: &str, data: HashMap<&str, &str>) -> Result<Vec<u8>, Error> {
        //TODO reuse connection
        let mut easy = Easy::new();
        //TODO handle all errors
        easy.url(url)?;
        easy.post(true)?;

        let data = Self::url_encode(data);
        easy.post_field_size(data.len() as u64)?;

        let mut response = Vec::new();
        {
            let mut transfer = easy.transfer();
            transfer.read_function(|buf| {
                data.as_bytes().read(buf).map_err(|_| ReadError::Abort)
            })?;

//            println!("http data {:?}", data);

            transfer.write_function(|data| {
//                if !self.aborted.get() {
                response.extend_from_slice(data);
                Ok(data.len())
//                } else {
//                    Err(WriteError::Pause)
//                }
            })?;
            transfer.perform()?;
        }

        Ok(response)
    }
}

impl Http for CurlHttp {
    fn post(&self, url: &str, data: HashMap<&str, &str>) -> Result<Vec<u8>, String> {
        self.post_inner(url, data).map_err(|e| {
            let description = error::Error::description(&e);
            let code = e.code();

            format!("{} (libcurl error {})", description, code)
        })
    }

    fn abort(&self) {
        self.pending_abort.set(true)
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use crate::http::CurlHttp;
    use hamcrest::prelude::*;

    #[test]
    fn url_encode_empty() {
        let data = HashMap::new();
        let encoded = CurlHttp::url_encode(data);
        assert_eq!("", encoded.as_str())
    }

    #[test]
    fn url_encode_one_field() {
        let data = hash_map! {"a" => "b"};
        let encoded = CurlHttp::url_encode(data);

        assert_eq!("a=b", encoded.as_str())
    }

    #[test]
    fn url_encode_multiple_fields() {
        let data = hash_map! {"a" => "b", "c" => "d"};
        let encoded = CurlHttp::url_encode(data);

        assert_that!(encoded.as_str(), any_of!(is(equal_to("a=b&c=d")), is(equal_to("c=d&a=b"))));
    }

    #[test]
    fn trim() {
        let data = hash_map! {"a" => "bc\n"};
        let encoded = CurlHttp::url_encode(data);

        assert_eq!("a=bc", encoded.as_str())
    }

    #[test]
    fn handle_whitespaces() {
        let data = hash_map! {"a" => "b c\td"};
        let encoded = CurlHttp::url_encode(data);

        assert_eq!("a=b+c+d", encoded.as_str())
    }
}
