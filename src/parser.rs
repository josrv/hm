use crate::dto::Link;
use std::collections::HashMap;
use quick_xml::Reader;
use quick_xml::events::Event;
use crate::parser::ParsingState::{Skipping, ReadingSnippet, ReadingTitle, ReadingPaging};
use std::io::BufReader;
use std::convert::TryFrom;
use std::borrow::Cow;

pub struct Html<'a>(pub &'a [u8]);

#[derive(Default)]
pub struct Links {
    pub links: Vec<Link>,
    pub paging: Option<Paging>,
}

#[derive(Debug, Default, PartialEq)]
pub struct Paging {
    pub s: i32,
    pub dc: i32,
}

#[derive(Debug, PartialEq)]
enum ParsingState {
    Skipping,
    ReadingTitle,
    ReadingSnippet,
    ReadingPaging { paging: Paging },
}

impl<'a> TryFrom<Html<'a>> for Links {
    type Error = String;

    fn try_from(html: Html<'a>) -> Result<Self, Self::Error> {
        let mut links = vec![];
        let mut reader = Reader::from_reader(BufReader::new(html.0));
        reader.check_end_names(false); // to support unclosed tags like <br>
        reader.expand_empty_elements(true); // to support self-closing tags like <input>

        let mut state = Skipping;
        let mut url = None;
        let mut title = String::with_capacity(75);
        let mut snippet = String::with_capacity(400);

        //FIXME restrict buffer length (with BufReader buffer capacity?)
        let mut buf = Vec::new();
        loop {
            match reader.read_event(&mut buf) {
                Ok(Event::Start(ref e)) => {
                    let tag_name = reader.decode(e.local_name());

                    // all needed information is in <a> and <input> tags
                    if !(tag_name == "a" || tag_name == "input") {
                        continue;
                    }

                    let attrs: HashMap<Cow<str>, String> = e.attributes()
                        .filter_map(|attr| {
                            attr.ok().and_then(|a| {
                                let key = reader.decode(a.key);

                                let value = reader.decode(a.value.as_ref());
                                //FIXME how do I not allocate here?
                                Some((key, (*value).to_owned()))
                            })
                        }).collect();

                    if let Some(c) = attrs.get("class") {
                        match c.as_ref() {
                            "result__a" => {
                                url = attrs.get("href").cloned();
                                state = ReadingTitle;
                            }
                            "result__snippet" => {
                                state = ReadingSnippet;
                            }
                            _ => {}
                        }
                    }

                    // parse paging info
                    if tag_name == "input" {
                        let type_and_value =
                            (attrs.get("type").map(String::as_str),
                             attrs.get("value").map(String::as_str));

                        if let (Some("submit"), Some("Next")) = type_and_value {
                            state = ReadingPaging {
                                paging: Paging { s: 0, dc: 0 }
                            }
                        };

                        if let Some(n) = attrs.get("name") {
                            // assuming the input fields are always in the same order
                            match n.as_ref() {
                                "s" => {
                                    let value = attrs
                                        .get("value")
                                        .ok_or_else(|| "No value")?
                                        .parse()
                                        .map_err(|_| "Can't parse [input.name=\"s\"] value")?;

                                    if let ReadingPaging { ref mut paging } = state {
                                        paging.s = value;
                                    };
                                }
                                "dc" => {
                                    let value = attrs
                                        .get("value")
                                        .ok_or_else(|| "No value")?
                                        .parse()
                                        .map_err(|_| "Can't parse [input.name=\"dc\"] value")?;

                                    if let ReadingPaging { ref mut paging } = state {
                                        paging.dc = value;
                                    }
                                }
                                _ => {}
                            };
                        }
                    }
                }

                // title and snippet may be split into multiple tags (e.g. <b>)
                Ok(Event::Text(ref bytes)) if state == ReadingTitle => {
                    let next_part = reader.decode(bytes);
                    title.push_str(next_part.as_ref());
                }
                Ok(Event::Text(ref bytes)) if state == ReadingSnippet => {
                    let next_part = reader.decode(bytes);
                    snippet.push_str(next_part.as_ref());
                }
                Ok(Event::End(ref e)) => {
                    let tag_name = reader.decode(e.local_name());
                    if "a" == tag_name.as_ref() {
                        if state == ReadingSnippet {
                            links.push(Link {
                                //TODO should I clone?
                                url: url.clone().unwrap(),
                                title: title.clone(),
                                snippet: snippet.clone(),
                            });

                            url = None;
                            title = String::with_capacity(75);
                            snippet = String::with_capacity(400);
                        }
                        state = Skipping;
                    }
                }
                Ok(Event::Eof) => break,//TODO I can stop parsing way earlier than EOF
                _ => {}
            }
        }

        let paging = match state {
            ReadingPaging { paging } => Some(paging),
            _ => None
        };

        //TODO produce Err if this is not a valid results page
        Ok(Links {
            links: links.to_owned(),
            paging,
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn parses_no_links() {
        let html = Html(
            br#"
            <div class="results">
            </div>
            "#
        );

        let links = Links::try_from(html).unwrap().links;

        assert_eq!(0, links.len())
    }

    #[test]
    fn parses_one_link() {
        let html = Html(
            br#"
            <div class="results">
                <div class="result">
                    <a class="result__a" href="https://link.com">Link</a>
                    <a class="result__snippet">Snippet</a>
                </div>
            </div>
            "#
        );

        let links = Links::try_from(html).unwrap().links;

        assert_eq!(1, links.len());
        let link = links.first().unwrap();
        assert_eq!("Link", link.title);
        assert_eq!("https://link.com", link.url);
        assert_eq!("Snippet", link.snippet);
    }

    #[test]
    fn parses_multiple_links() {
        let html = Html(
            br#"
            <div class="results">
                <div class="result">
                    <a class="result__a" href="https://link1.com">Link 1</a>
                    <a class="result__snippet">Snippet 1</a>
                </div>
                <div class="result">
                    <a class="result__a" href="https://link2.com">Link 2</a>
                    <a class="result__snippet">Snippet 2</a>
                </div>
            </div>
            "#
        );

        let links = Links::try_from(html).unwrap().links;

        assert_eq!(2, links.len());
        let link1 = &links[0];
        assert_eq!("Link 1", link1.title);
        assert_eq!("https://link1.com", link1.url);
        assert_eq!("Snippet 1", link1.snippet);

        let link2 = &links[1];
        assert_eq!("Link 2", link2.title);
        assert_eq!("https://link2.com", link2.url);
        assert_eq!("Snippet 2", link2.snippet);
    }

    #[test]
    fn handles_inner_snippet_tags() {
        let html = Html(
            br#"
            <div class="results">
                <div class="result">
                    <a class="result__a" href="https://link.com">Link</a>
                    <a class="result__snippet">A <b>snippet</b> with some inner tags</a>
                </div>
            </div>
            "#
        );

        let links = Links::try_from(html).unwrap().links;

        assert_eq!(1, links.len());
//TODO add assertions after designing the handling of the bold text
    }

    #[test]
    fn parses_next_paging() {
        let html = Html(
            br#"
            <div class="results">
                <div class="result">
                    <a class="result__a" href="https://link.com">Link</a>
                    <a class="result__snippet">A <b>snippet</b> with some inner tags</a>
                </div>
                <div class="nav-link">
                    <input type="submit" value="Next">
                    <input type="hidden" name="q" value="test">
                    <input type="hidden" name="s" value="30">
                    <input type="hidden" name="dc" value="29">
                </div>
            </div>
            "#
        );

        let paging = Links::try_from(html).unwrap().paging.unwrap();

        assert_eq!(30, paging.s);
        assert_eq!(29, paging.dc);
    }

    #[test]
    fn parses_prev_and_next_paging() {
        let html = Html(
            br#"
            <div class="results">
                <div class="result">
                    <a class="result__a" href="https://link.com">Link</a>
                    <a class="result__snippet">A <b>snippet</b> with some inner tags</a>
                </div>
                <div class="nav-link">
                    <input type="submit" value="Previous">
                    <input type="hidden" name="q" value="test">
                    <input type="hidden" name="s" value="0">
                    <input type="hidden" name="dc" value="-31">
                </div>
                <div class="nav-link">
                    <input type="submit" value="Next">
                    <input type="hidden" name="q" value="test">
                    <input type="hidden" name="s" value="80">
                    <input type="hidden" name="dc" value="79">
                </div>
            </div>
            "#
        );

        let paging = Links::try_from(html).unwrap().paging.unwrap();

        assert_eq!(80, paging.s);
        assert_eq!(79, paging.dc);
    }

    #[test]
    fn parses_no_paging_from_the_last_page() {
        let html = Html(
            br#"
            <div class="results">
                <div class="result">
                    <a class="result__a" href="https://link.com">Link</a>
                    <a class="result__snippet">A <b>snippet</b> with some inner tags</a>
                </div>
                <div class="nav-link">
                    <input type="submit" value="Previous">
                    <input type="hidden" name="q" value="test">
                    <input type="hidden" name="s" value="0">
                    <input type="hidden" name="dc" value="-31">
                </div>
            </div>
            "#
        );

        let paging = Links::try_from(html).unwrap().paging;

        assert_eq!(None, paging);
    }
}
