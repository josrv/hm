use std::char;
use std::collections::HashMap;

#[derive(Debug, PartialEq)]
pub struct Alphabet {
    pub chars: Vec<char>,
    indices: HashMap<char, usize>,
}

impl From<Vec<char>> for Alphabet {
    fn from(chars: Vec<char>) -> Self {
        let mut indices = HashMap::new();
        chars.iter().enumerate().for_each(|(i, c)| {
            if !indices.contains_key(c) {
                indices.insert(*c, i);
            }
        });

        Self {
            chars,
            indices,
        }
    }
}

impl From<&str> for Alphabet {
    fn from(str: &str) -> Self {
        let alphabet: Vec<char> = str.chars().collect();
        Self::from(alphabet)
    }
}

impl Alphabet {
    pub fn key_size(&self, n: usize) -> usize {
        (n / (self.chars.len() + 1)) + 1
    }

    pub fn char_index(&self, c: char) -> Option<usize> {
        self.indices.get(&c).cloned()
    }

    pub fn has_char(&self, c: char) -> bool {
        self.char_index(c).is_some()
    }
}

/// Generate the given number of words from the given alphabet.
pub fn words(n: usize, alphabet: &Alphabet) -> Vec<String> {
    //TODO cache words
    let mut words = Vec::new();
    let word_size = alphabet.key_size(n);

    let mut indices = vec![0; word_size];

    for _words_left in 0..n {
        // construct a word
        let mut word = String::new();
        for index in indices.iter() {
            word.push(alphabet.chars[*index]);
        }
        words.push(word);

        // update indices and digit overflows
        indices[word_size - 1] += 1;
        for i in (0..word_size).rev() {
            if indices[i] == alphabet.chars.len() {
                indices[i] = 0;
                if i > 0 {
                    indices[i - 1] += 1;
                }
            }
        }
    }

    words
}

pub fn word_index(word: &str, alphabet: &Alphabet) -> Result<usize, String> {
    let alphabet_size = alphabet.chars.len();
    let word_size = word.len();

    word.char_indices().fold(Ok(0), |acc, (i, c)| {
        acc.and_then(|acc| {
            let char_index = alphabet.char_index(c);
            match char_index {
                None => Err(format!("The alphabet doesn't contain the character {}.", c)),
                Some(index) => Ok(acc + index * alphabet_size.pow((word_size - i - 1) as u32))
            }
        })
    })
}

#[macro_export]
macro_rules! hash_map (
    { $($key:expr => $value:expr),+ } => {
        {
            let mut m = ::std::collections::HashMap::new();
            $(
                m.insert($key, $value);
            )+
            m
        }
     };
);

#[cfg(test)]
mod tests {
    use crate::utils::words;
    use crate::utils::word_index;
    use crate::utils::Alphabet;

    #[test]
    fn n_less_than_alphabet_size() {
        let alphabet = Alphabet::from("abcd");
        let words = words(3, &alphabet);

        assert_eq!(vec![String::from("a"), String::from("b"), String::from("c")], words);
    }

    #[test]
    fn n_equal_to_alphabet_size() {
        let alphabet = Alphabet::from("abcd");
        let words = words(4, &alphabet);

        assert_eq!(vec![String::from("a"),
                        String::from("b"),
                        String::from("c"),
                        String::from("d")], words);
    }

    #[test]
    fn n_more_than_alphabet_size() {
        let alphabet = Alphabet::from("abcd");
        let words = words(5, &alphabet);

        assert_eq!(vec![String::from("aa"), String::from("ab"), String::from("ac"), String::from("ad"), String::from("ba")], words);
    }

    #[test]
    fn index_from_simple_word() {
        let alphabet = Alphabet::from("abcd");

        assert_eq!(Ok(1), word_index("b", &alphabet))
    }

    #[test]
    fn index_from_multichar_word() {
        let alphabet = Alphabet::from("abcd");

        assert_eq!(Ok(11), word_index("cd", &alphabet))
    }

    #[test]
    fn index_from_word_with_unknown_char() {
        let alphabet = Alphabet::from("abcd");

        assert!(word_index("w", &alphabet).is_err())
    }
}

