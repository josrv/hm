use std::io::{stdout, Write};
use std::process::Command;
use crate::utils::word_index;
use std::sync::mpsc::{Sender, Receiver};
use crate::fetcher;
use crate::viewer::ViewerState::{ReadingQuery, WaitingForResults, ShowingResults};
use std::thread;
use crate::dto::Link;
use std::sync::{Arc, Mutex};
use arl::{Arl, Input, Key};
use crate::display::LinksRepr;
use crate::config::Config;
use crate::fetcher::FetchResult;
use crate::utils;
use std::cmp::min;

#[derive(PartialEq, Debug, Clone)]
enum ViewerState {
    ReadingQuery,
    WaitingForResults { query: String, offset: u16 },
    ShowingResults {
        query: String,
        links: Vec<Link>,
        offset: u16,
        has_more: bool,
        current_key: String,
    },
}

pub struct Viewer {
    state: Arc<Mutex<ViewerState>>,
    ctrl_tx: Sender<fetcher::Command>,
}

const PROMPT: &str = "ddg> ";
const RESULTS_TEXT: &str = "Type result key or <Esc> to start a new search";

impl Viewer {
    pub fn new(ctrl_tx: Sender<fetcher::Command>) -> Self {
        Viewer {
            state: Arc::new(Mutex::new(ViewerState::ReadingQuery)),
            ctrl_tx,
        }
    }

    fn load_next_results(&mut self, limit: u8) {
        let cur_state: Option<(String, u16)> = {
            let mut state = self.state.lock().unwrap();
            match *state {
                ShowingResults { has_more: false, .. } => {
                    // can't fetch more
                    None
                }
                ShowingResults {
                    ref mut query, ref offset, ..
                } => Some((query.drain(..).collect(), *offset)),
                _ => None
            }
        };

        if let Some((mut query, offset)) = cur_state {
            self.change_state(ViewerState::WaitingForResults {
                query: query.drain(..).collect(),
                offset: offset + u16::from(limit),
            });
        }
    }

    fn load_prev_results(&mut self, limit: u8) {
        let cur_state: Option<(String, u16)> = {
            let mut state = self.state.lock().unwrap();
            match *state {
                ShowingResults { offset: 0, .. } => {
                    // already in the beginning
                    None
                }
                ShowingResults {
                    ref mut query, ref offset, ..
                } => Some((query.drain(..).collect(), *offset)),
                _ => None
            }
        };

        if let Some((mut query, offset)) = cur_state {
            self.change_state(ViewerState::WaitingForResults {
                query: query.drain(..).collect(),
                offset: min(0, offset - u16::from(limit)),
            });
        }
    }

    fn change_state(&self, new_state: ViewerState) {
        let mut state = self.state.lock().unwrap();
        match new_state {
            WaitingForResults { ref query, ref offset } =>
                self.ctrl_tx.send(fetcher::Command::Query {
                    query: query.to_owned(),
                    offset: *offset,
                }).unwrap(),

            ReadingQuery => {
                Self::print_prompt();
            }
            _ => ()
        }

        *state = new_state;
    }

    fn print_prompt() {
        print!("{}", PROMPT);
        stdout().lock().flush().unwrap();
    }

    pub fn start(&mut self, config: Arc<Config>, ui_rx: Receiver<Result<FetchResult, String>>) {
        Self::print_prompt();
        let arl = Arc::new(Mutex::new(Arl::new()));

        let state = Arc::clone(&self.state);
        let arl_inner = Arc::clone(&arl);
        let config_inner = Arc::clone(&config);
        thread::spawn(move || {
            for links in ui_rx {
                let mut state = state.lock().unwrap();
                if let ViewerState::WaitingForResults { ref mut query, ref offset } = *state {
                    match links {
                        Ok(FetchResult { links, has_more }) => {
                            // links are successfully fetched, preparing to show
                            let keys = utils::words(links.len(), &config_inner.keys);

                            print!("{}", LinksRepr {
                                links: links.as_slice(),
                                keys: Some(keys),
                            });
                            println!("{}", RESULTS_TEXT);

                            *state = ShowingResults {
                                query: query.drain(..).collect(),
                                links,
                                offset: *offset,
                                has_more,
                                current_key: String::new(),
                            };
                        }
                        Err(e) => {
                            eprintln!("{}", e);
                            Self::print_prompt();

                            arl_inner.lock().unwrap().line_mode();

                            *state = ReadingQuery;
                        }
                    }
                }
            }
        });

        //TODO locking two times
        let inputs = arl.lock().unwrap().start();
        arl.lock().unwrap().line_mode();

        for input in inputs {
            match input {
                Input::Line(query) => {
                    // a new query is submitted
                    self.change_state(ViewerState::WaitingForResults { query, offset: 0 });
                    arl.lock().unwrap().symbol_mode();
                }
                Input::Symbol(Key::Char('q')) => {
                    break;
                }
                Input::Symbol(Key::Esc) => {
                    self.change_state(ViewerState::ReadingQuery);
                    //TODO abort http
                    arl.lock().unwrap().line_mode();
                }
                Input::Symbol(Key::Char('n')) => {
                    self.load_next_results(config.limit);
                }
                Input::Symbol(Key::Char('p')) => {
                    self.load_prev_results(config.limit);
                }
                Input::Symbol(Key::Char(c)) => {
                    // choosing a link to open
                    let mut state = self.state.lock().unwrap();
                    if let ShowingResults {
                        ref query,
                        ref offset,
                        ref has_more,
                        ref links,
                        ref current_key
                    } = *state {
                        if !config.keys.has_char(c) {
                            // ignore unrelated chars
                            continue;
                        }

                        let mut current_key = current_key.clone();

                        let key_size = config.keys.key_size(links.len());
                        current_key.push(c);
                        if current_key.len() == key_size {
                            // open selected link
                            let index = word_index(current_key.as_str(), &config.keys);

                            if let Ok(index) = index {
                                let mut url = links.get(index as usize);
                                if let Some(Link { url, .. }) = url.take() {
                                    Command::new(config.command.clone())
                                        .arg(url.as_str())
                                        .spawn()
                                        .unwrap();
                                }
                            }

                            current_key.clear();
                        }

                        //TODO Clone trait
                        *state = ShowingResults {
                            query: query.clone(),
                            links: links.clone(),
                            offset: *offset,
                            has_more: *has_more,
                            current_key,
                        };
                    }
                }
                _ => ()
            }
        }

        arl.lock().unwrap().line_mode();
    }
}

